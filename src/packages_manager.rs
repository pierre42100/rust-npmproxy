//! # Package manager
//!
//! @author Pierre Hubert

use std::path::PathBuf;

use crate::config::conf;
use crate::utils::error::Res;

fn get_package_file(p: &str) -> PathBuf {
    conf().storage_path().join(p).join("package.json")
}

/// Update packages information
pub fn update(p: &str) -> Res {
    let path = get_package_file(p);

    if let Some(parent) = path.parent() {
        std::fs::create_dir_all(parent)?;
    }

    let url = format!("{}{}", conf().upstream, p);

    let package = reqwest::blocking::get(url)?.text()?;

    std::fs::write(path, package)?;

    Ok(())
}

/// Get package information adapted for the proxy
pub fn get_adapted(p: &str) -> Res<String> {
    let path = get_package_file(p);
    let package = std::fs::read_to_string(path)?;

    // Adapt content
    let package = package.replace(&conf().upstream, &conf().access_url);

    Ok(package)
}