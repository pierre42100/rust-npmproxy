//! # NPM Proxy service utilities
//!
//! @author Pierre Hubert

use crate::data::http_error::HttpError;
use std::fmt::Display;

pub type Res<E = ()> = Result<E, ExecError>;

#[derive(Debug, Clone)]
pub struct ExecError(String);

impl ExecError {
    pub fn new(msg: &str) -> Self {
        Self(msg.to_string())
    }

    pub fn to_string(&self) -> String {
        self.0.clone()
    }
}

pub fn new_err(msg: &str) -> Res {
    return Err(ExecError(msg.to_string()));
}

impl<T: Display> From<T> for ExecError {
    fn from(e: T) -> Self {
        ExecError(format!("Error: {}", e))
    }
}

impl From<ExecError> for actix_web::Error {
    fn from(e: ExecError) -> Self {
        HttpError::server_error(&e.0).into_error()
    }
}