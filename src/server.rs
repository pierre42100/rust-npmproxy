//! # Server
//!
//! Where all the logic takes place
//!
//! @author Pierre Hubert

use actix_web::{App, HttpRequest, HttpResponse, HttpServer, web};

use crate::{artifacts_manager, packages_manager};
use crate::config::conf;
use crate::data::artifact_reference::ArtifactReference;

async fn hello() -> HttpResponse {
    HttpResponse::Ok().body("NPM Proxy server")
}

async fn get_package(uri: &str) -> HttpResponse {
    let split = uri.split("/-/").collect::<Vec<&str>>();
    let package = &split[0].replace("../", "")[1..];
    let package = percent_encoding::percent_decode_str(package).decode_utf8_lossy();
    let file = split[1];

    if !package.starts_with("@") && package.contains("/") {
        return HttpResponse::BadRequest().body("Scoped package must start with a '@'!");
    }

    let artifact_path = match artifacts_manager::get_artifact(&ArtifactReference {
        package: package.to_string(),
        file: file.to_string(),
    }) {
        Ok(p) => p,
        Err(e) => {
            eprintln!("Error while getting artifact: {}",
                      e.to_string());
            return HttpResponse::InternalServerError().body("Failed to load package!");
        }
    };

    let bytes = match std::fs::read(artifact_path) {
        Ok(b) => b,
        Err(e) => {
            eprintln!("Error while reading artifact: {}", e.to_string());
            return HttpResponse::InternalServerError().body("Failed to load package!");
        }
    };


    HttpResponse::Ok().content_type("application/x-compressed-tar").body(bytes)
}

async fn handle_request(r: HttpRequest) -> HttpResponse {
    let uri = r.uri().path();

    // Check if the user wants to download a package
    if uri.ends_with(".tgz") && uri.contains("/-/") {
        return get_package(uri).await;
    }

    // Get package
    let package = percent_encoding::percent_decode_str(&uri[1..]).decode_utf8_lossy();
    let package = package.replace("//", "/").replace("../", "/");

    if !package.contains("@") && package.contains("/") {
        return HttpResponse::BadRequest().body("Scoped package must start with a '@'!");
    }

    if let Err(e) = packages_manager::update(&package) {
        eprintln!("Failed to update package information! Trying to continue anyway... {}", e.to_string());
    }

    let res = match packages_manager::get_adapted(&package) {
        Ok(s) => s,
        Err(e) => {
            eprintln!("Failed to load package information! {}", e.to_string());
            return HttpResponse::InternalServerError().body("Failed to load package information!");
        }
    };

    HttpResponse::Ok().content_type("application/json").body(res)
}


/// Start the server
pub async fn start_server() -> std::io::Result<()> {
    println!("Starting to listen on http://{}/", conf().listen_address);

    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(hello))
            .route("**", web::get().to(handle_request))
    })
        .bind(&conf().listen_address)?
        .run()
        .await
}