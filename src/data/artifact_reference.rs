//! Artifact reference

pub struct ArtifactReference {
    pub package: String,
    pub file: String,
}