//! # NPM artifacts manager
//!
//! @author Pierre Hubert

use std::path::PathBuf;

use crate::config::conf;
use crate::data::artifact_reference::ArtifactReference;
use crate::utils::error::{ExecError, Res};

fn get_artifact_path(art: &ArtifactReference) -> PathBuf {
    return conf().storage_path().join(&art.package).join(&art.file);
}

/// Get an artifact. If it is available locally, return the cached version. Otherwise, download
/// it from upstream source
pub fn get_artifact(art: &ArtifactReference) -> Res<PathBuf> {
    let path = get_artifact_path(art);

    // Check if the artifact is already available
    if path.exists() {
        return Ok(path);
    }

    // Create parent directory, if required
    let parent = path.parent()
        .ok_or(ExecError::new("Could not get parent directory!"))?;

    if !parent.exists() {
        std::fs::create_dir_all(parent)?;
    }

    // Download the artifact from upstream source
    let url = format!("{}{}/-/{}", conf().upstream, art.package, art.file);

    println!("Downloading new artifact from {}", url);

    // Download artifact
    let bytes = reqwest::blocking::get(url)?.bytes()?;
    std::fs::write(&path, bytes)?;

    return Ok(path);
}